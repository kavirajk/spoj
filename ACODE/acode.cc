// Author: kaviraj
// Tag: DP
#include <iostream>
#include <cstdio>
#include <cstring>

using namespace std;

long long dp[5001];

int main() {
  string s;
  memset(dp,0,sizeof(dp));
  while(cin>>s && s!="0") {
    dp[0]=1;
    long long tmp;
    tmp=s[0]-'0';
    tmp*=10;
    tmp+=s[1]-'0';
    if(tmp<=26)
      dp[1]=2;
    else
      dp[1]=1;

    if(s[1]=='0')
      dp[1]-=1;

    for(int i=2;i<s.size();++i) {
      tmp=s[i-1]-'0';
      tmp*=10;
      tmp+=s[i]-'0';

      if(tmp<=26)
	dp[i]=dp[i-1]+dp[i-2];
      else
	dp[i]=dp[i-1];

      if(s[i]=='0')
	dp[i]-=dp[i-1];

      if(s[i-1]=='0')
	dp[i]-=dp[i-2];

    }
    cout<<dp[s.size()-1]<<endl;
  }
  return 0;
}
