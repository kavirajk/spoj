// Author: kaviraj
// Tag: DP

#include <iostream>
#include <cstdio>
#include <cstring>

using namespace std;

long long dp[5001];

int main() {
  string s;
  while(cin>>s && s!="0") {
    memset(dp,0,sizeof(dp));
    dp[0]=1;
    int n=s.size();
    for(int i=1;i<n;++i) {
      int tmp=(s[i-1]-'0')*10;
      tmp+=s[i]-'0';

      if(s[i]-'0') // in case of '0' in s[i]
	dp[i]=dp[i-1];
      if(tmp<=26 && tmp>9) // in case of "04" 
	dp[i]+=dp[(i-2)<0?0:(i-2)];
    }
    cout<<dp[n-1]<<endl;
  }
  return 0;
}
