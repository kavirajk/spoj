#include <cstdio>
#include <iostream>
#include <algorithm>

using namespace std;

int main() {
  int n;
  while(cin>>n && n) {
    string s;
    cin>>s;
    int r=s.size()/n;
    char ch[r][n];
    int k=0;
    for(int i=0;i<r;++i) {
      for(int j=0;j<n;++j) {
	ch[i][j]=s[k++];
      }
    }

    for(int i=0;i<r;++i) {
      if(i%2) {
	reverse(ch[i]+0,ch[i]+n);
      }
    }

    string ans="";
    for(int i=0;i<n;++i) {
      for(int j=0;j<r;++j) {
	//	if(ch[j][i]!='x')
	  ans+=ch[j][i];
      }
    }
    cout<<ans<<endl;
  }
  return 0;
}
