#include <iostream>
#include <cstdio>

using namespace std;

int main() {
  int g,b;
  while(cin>>g>>b) {
    if(g==-1 && b==-1)
      break;
    int m=min(g,b);
    int n=max(g,b);

    cout<<int(float(n)/(m+1)+0.5)<<endl;

  }
  return 0;
}
