#include <iostream>
#include <cstdio>
#include <vector>

using namespace std;

void next(vector<int>& v,int start,int end) {
  if(start<0) {
    v[v.size()-1]=1;
    v.insert(v.begin(),1);
    return;
  } else if(v[start]<9) {
    v[start]=v[end]=v[start]+1;
    return;
  } else {
    v[start]=v[end]=0;
    next(v,start-1,end+1);
    return;
  }
}

int main() {
  int t;
  cin>>t;
  while(t--) {
    string s;
    cin>>s;
    vector<int> v;
    int n=s.size()-1;
    for(int i=0;i<s.size();++i) {
      v.push_back(s[i]-'0');
    }
    next(v,n/2,(n-(n/2)));
    for(int i=0;i<v.size();++i) {
      cout<<v[i];
    }
    cout<<endl;
  }
  return 0;
}
