#include <iostream>
#include <cstdio>

using namespace std;

int fastPow(int a,int b) {
  if(b==0)
    return 1;
  if(b==1)
    return a%10;
  int ans=fastPow((a*a)%10,b/2);
  if(b%2)
    ans=(ans*a)%10;

  return ans;
}

int main() {
  int t;
  cin>>t;
  while(t--) {
    int a,b;
    cin>>a>>b;
    cout<<fastPow(a,b)<<endl;
  }
  return 0;
}
