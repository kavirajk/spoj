#include <iostream>
#include <cstdio>

using namespace std;

int main() {
  int t;
  cin>>t;
  while(t--) {
    int n;
    cin>>n;
    cout<<192+(n-1)*250<<endl;
  }
  return 0;
}
