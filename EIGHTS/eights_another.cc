#include <iostream>
#include <cstdio>

using namespace std;

int main() {
  int t;
  cin>>t;
  while(t--) {
    long long n;
    cin>>n;

    long long t=(5*(n-1)+3)/2;
    t*=100;
    if(n%2) {
      t+=92;
    } else {
      t+=42;
    }
    cout<<t<<endl;
  }
  return 0;
}
