#include <iostream>
#include <vector>
#include <algorithm>
#include <cstdio>

using namespace std;

int main() {
  int a,d;
  while(cin>>a>>d && (a!=0 || d!=0)) {
    vector<int> ap(a),dp(d);
    for(int i=0;i<a;++i) {
      cin>>ap[i];
    }
    for(int i=0;i<d;++i) {
      cin>>dp[i];
    }
    sort(ap.begin(),ap.end());
    sort(dp.begin(),dp.end());

    int i;

    for(i=0;i<ap.size();++i) {
      if(ap[i] < dp[1]) {
	cout<<"Y"<<endl;
	break;
      }
    }
    if(i==ap.size())
      cout<<"N\n";
  }
  return 0;
}
