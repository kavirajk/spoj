// Author: kaviraj
// Tag: adhoc

#include <iostream>
#include <cstdio>
#include <vector>
#include <algorithm>

using namespace std;

int main() {
  int t;
  cin>>t;
  for(int j=1;j<=t;++j) {
    int b,n;
    cin>>b>>n;
    vector<int> v(n);
    for(int i=0;i<n;++i) {
      cin>>v[i];
    }
    sort(v.begin(),v.end(),greater<int>());
    int sum=0,i;
    for(i=0;i<n;++i) {
      sum+=v[i];
      if(sum>=b)
	break;
    }
    cout<<"Scenario #"<<j<<":\n";
    if(i==n)
      cout<<"impossible\n";
    else
      cout<<i+1<<endl;
    cout<<endl;
  }
  return 0;
}
