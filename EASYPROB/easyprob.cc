#include <iostream>
#include <cstdio>
#include <sstream>

typedef unsigned long long int64;

using namespace std;

int64 calc(int64 x) {
  int64 y=0;
  x/=2;
  while(x) {
    y++;
    x/=2;
  }
  return y;
}

string powerof2(int64 x) {
  if(x==1) {
   return "2(0)";
  }else if(x==2) {
    return "2";
  }else if(x==3) {
    return "2+2(0)"; // if exclude this, then 2^3 = 2(2(0)) + 2 instead of 2^3=2+2(0)
  }else {
    int64 y=calc(x);
    stringstream ss;
    if(x!=(1<<y)) // if no check then need to handle x=0 basecase if x==(1<<y)
      ss<<"2("<<powerof2(y)<<")"<<"+"<<powerof2(x-(1<<y));
    else
      ss<<"2("<<powerof2(y)<<")";
    return ss.str();
  }
}

int main() {
  //  freopen("output.txt","w",stdout);
  string s="";
  int64 n;
  while(cin>>n) {
    cout<<n<<"="<<powerof2(n)<<endl;
  }
  return 0;
}
