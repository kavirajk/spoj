// Author: kaviraj
// Tag: DP

#include <iostream>
#include <cstdio>
#include <cstring>
#include <map>

using namespace std;

map<int,int> dp;

int fn(int n) {

  //cout<<"n: "<<n<<endl;

  if(n==0)
    return 0;
  if(dp[n]!=0)
    return dp[n];

  int t=fn(n/2)+fn(n/3)+fn(n/4);
  if(t>n)
    dp[n]=t;
  else
    dp[n]=n;
  
  return dp[n];
}

int main() {
  int n;
  while(cin>>n) {
    cout<<fn(n)<<endl;
  }
  return 0;
}
