// TASK: ADDREV
// AUTHOR: k4v1r4j

#include<iostream>
#include<cstdio>

using namespace std;

int rever(int a) {
  int b=0;
  while(a%10==0)
    a/=10;
  while(a!=0) {
    b=(b*10)+a%10;
    a/=10;
  }
  return b;
}

int main() {
  int T;
  int a,b;
  cin>>T;
  while(T--) {
    cin>>a>>b;
    cout<<rever(rever(a)+rever(b))<<endl;
  }
  return 0;
}
