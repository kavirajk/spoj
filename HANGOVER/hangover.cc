#include <iostream>
#include <cstdio>

using namespace std;

int main() {
  float c;
  while(cin>>c && c) {
    float sum=0;
    int i=2;
    while(sum<c) {
      sum+=1.0/i;
      i++;
    }
    cout<<i-2<<" card(s)"<<endl;
  }
  return 0;
}
