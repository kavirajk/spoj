#include <iostream>
#include <cstdio>

using namespace std;

int main() {
  long long  a,b,c;
  while(cin>>a>>b>>c) {
    if(!a && !b && !c)
      break;
    if(b-a == c-b) {
      cout<<"AP "<<c+(c-b)<<endl;
    } else {
      cout<<"GP "<<c*(c/b)<<endl;
    }
  }
  return 0;
}
