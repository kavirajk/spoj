#include <iostream>
#include <cstdio>

typedef long long int64;

using namespace std;


int main() {
  int t;
  cin>>t;
  for(int i=1;i<=t;++i) {
    int64 r;
    cin>>r;
    double ans=(4*r*r)+0.25;
    printf("Case %d: %.2f\n",i,ans);
  }
  return 0;
}
