#include <iostream>
#include <cstdio>
#include <cctype>

using namespace std;

bool contains_under(string str) {
  for(int i=0;i<str.size();++i) {
    if(str[i]=='_')
      return true;
  }
  return false;
}

bool contains_caps(string str) {
  for(int i=0;i<str.size();++i) {
    if(isupper(str[i]))
      return true;
  }
  return false;
}

string convert_to_cpp(string str) {
  string s="";
  for(int i=0;i<str.size();++i) {
    if(isupper(str[i])) {
      s+='_';
    } 
      s+=tolower(str[i]);
  }
  return s;
}

string convert_to_java(string str) {
  string s="";
  for(int i=0;i<str.size();++i) {
    if(str[i]=='_') {
      if(str[++i]=='_')
	return "Error!";
      s+=toupper(str[i]);
    } else
      s+=str[i];
  }
  return s;
}
int main() {

  string str;
  while(getline(cin,str)) {

    if(!islower(str[0])) {
      cout<<"Error!\n";
      continue;
    }

    int len=str.size();

    if(!islower(str[len-1])) {
      cout<<"Error!\n";
      continue;
    }
    
    if(contains_caps(str)) {
      if(contains_under(str)) {
	cout<<"Error!\n";
      } else 
	cout<<convert_to_cpp(str)<<endl;
    } else {
      cout<<convert_to_java(str)<<endl;
    }
  }
  return 0;
}
