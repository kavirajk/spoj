#include <cstdio>
002: #include <cstring>
003: #include <cctype>
004: 
005: #define MAX 200
006: 
007: const int STATUS_INIT = 1;
008: const int STATUS_JAVA_WORD = 2;
009: const int STATUS_CPP_UNDERSCORE = 3;
010: const int STATUS_JAVA_BIGLETTER = 4;
011: const int STATUS_ERR = 5;
012: const int STATUS_FIRST_WORD = 6;
013: const int STATUS_CPP_WORD = 7;
014: 
015: bool isSmallLetter(char character) {
016:  return character <= 'z' && character >= 'a';
017: }
018: 
019: bool isBigLetter(char character) {
020:  return character <= 'Z' && character >= 'A';
021: }
022: 
023: bool isUnderscore(char character) {
024:  return '_' == character;
025: }
026: 
027: bool convert(char* word, char* output) {
028:  int outputIdx = 0;
029:  int len = strnlen(word, MAX);
030:  int status = STATUS_INIT;
031: 
032:  for(int i=0; i<len; i++) {
033:   char current = word[i];
034:   switch(status) {
035:    case STATUS_INIT:
036:     if(isSmallLetter(current)) {
037:      status = STATUS_FIRST_WORD;
038:      output[outputIdx++] = current;
039:     } else {
040:      status = STATUS_ERR;
041:     }
042: 
043:     break;
044:    case STATUS_FIRST_WORD:
045:     if(isSmallLetter(current)) {
046:      output[outputIdx++] = current;
047:     } else if(isBigLetter(current)) {
048:      status = STATUS_JAVA_BIGLETTER;
049:      output[outputIdx++] = '_';
050:      output[outputIdx++] = tolower(current);
051:     } else if(isUnderscore(current)) {
052:      status = STATUS_CPP_UNDERSCORE;
053:     } else {
054:      status = STATUS_ERR;
055:     }
056:     break;
057: 
058:    case STATUS_CPP_WORD:
059:     if(isSmallLetter(current)) {
060:      output[outputIdx++] = current;
061:     } else if(isUnderscore(current)) {
062:                                         status = STATUS_CPP_UNDERSCORE;
063:     } else {
064:      status = STATUS_ERR;
065:     }
066: 
067:     break;
068: 
069:    case STATUS_CPP_UNDERSCORE:
070:     if(isSmallLetter(current)) {
071:      output[outputIdx++] = toupper(current);
072:      status = STATUS_CPP_WORD;
073:     } else {
074:      status = STATUS_ERR;
075:     }
076: 
077:     break;
078: 
079:    case STATUS_JAVA_BIGLETTER:
080:     if(isSmallLetter(current)) {
081:      output[outputIdx++] = current;
082:      status = STATUS_JAVA_WORD;
083:     } else if(isBigLetter(current)) {
084:      output[outputIdx++] = '_';
085:      output[outputIdx++] = tolower(current);
086:     } else {
087:      status = STATUS_ERR;
088:     }
089: 
090:     break;
091: 
092:    case STATUS_JAVA_WORD:
093:     if(isSmallLetter(current)) {
094:      output[outputIdx++] = current;
095:     } else if(isBigLetter(current)) {
096:      status = STATUS_JAVA_BIGLETTER;
097:      output[outputIdx++] = '_';
098:      output[outputIdx++] = tolower(current);
099:     } else {
100:      status = STATUS_ERR;
101:     }
102: 
103:     break;
104: 
105:    default:
106:    case STATUS_ERR:
107:     return false;
108:   }
109:  } 
110: 
111:  return status == STATUS_JAVA_WORD || 
112:   status == STATUS_CPP_WORD || 
113:   status == STATUS_FIRST_WORD || 
114:   status == STATUS_JAVA_BIGLETTER;
115: }
116: 
117: int main(int argc, char** argv) {
118: 
119:  char word[MAX+1];
120:  char output[MAX+1];
121: 
122:  memset(word, 0, MAX+1);
123:  memset(output, 0, MAX+1);
124: 
125:  while(scanf("%s",word) > 0) {
126:   bool converted = convert(word, output);
127:   if(converted) {
128:    printf("%s\n", output);
129:    } else {
130:    printf("Error!\n");
131:   }
132:   memset(word, 0, MAX+1);
133:   memset(output, 0, MAX+1);
  }
 
  return 0;
 }
