// Author: kaviraj
// Tag: adhoc

// cin, cout wont work. TLE.
// so using C I/O

#include <iostream>
#include <cstdio>
#include <vector>
#include <algorithm>

using namespace std;

int main() {
  int t;
  cin>>t;
  while(t--) {
    int m,n;
    cin>>m>>n;
    int v1[m],v2[n];
    for(int i=0;i<m;++i) {
      scanf("%d",&v1[i]);
    }
    for(int i=0;i<n;++i) {
      scanf("%d",&v2[i]);
    }
    sort(v1,v1+m);
    sort(v2,v2+n);
    if(v1[m-1]<v2[n-1]) {
      printf("MechaGodzilla\n");
    } else {
      printf("Godzilla\n");
    }
  }
  return 0;
}
