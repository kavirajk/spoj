#include <iostream>
#include <vector>
#include <cstdio>
#include <iterator>
#include <algorithm>

using namespace std;

unsigned long long msort(vector<int>&);
unsigned long long merge(vector<int>& left,vector<int>& right,vector<int>& v);

int main() {

  int t;
  cin>>t;
  
  for(int i=0;i<t;++i) {
    int n;
    cin>>n;
    vector<int> v(n);
    for(int i=0;i<n;++i) {
      cin>>v[i];
    }
    unsigned long long c=msort(v);
    cout<<c<<endl;
  }
  return 0;
}

unsigned long long msort(vector<int>& v) {
  int len=v.size()/2;
  if(v.size()<=1)
    return 0;

  vector<int> left(v.begin(),v.begin()+len);
  vector<int> right(v.begin()+len,v.end());

  unsigned long long c=0;

  c+=msort(left);
  c+=msort(right);

  c+=merge(left,right,v);
  
  return c;
}

unsigned long long merge(vector<int>& left,vector<int>& right,vector<int>& v) {
  int ilen=left.size();
  int jlen=right.size();

  int klen=ilen+jlen;

  unsigned long long c=0;

  int i=0,j=0,k=0;
  for(;k<klen && i<ilen && j<jlen;++k) {
    if(left[i]<=right[j]) {
      v[k]=left[i];
      i++;
    } else {
      v[k]=right[j];
      j++;
      c+=(left.size()-i);
    }
  }

  if(k==klen)
    return c;

  if(i<ilen) {
    copy(left.begin()+i,left.end(),v.begin()+k);
  } else {
    copy(right.begin()+j,right.end(),v.begin()+k);
  }

  return c;
}
