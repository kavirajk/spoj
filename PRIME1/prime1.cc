#include <iostream>
#include <cstdio>
#include <cstring>
#include <vector>
#include <algorithm>
#include <iterator>

using namespace std;

const int limit=31623; // sqrt of 1 billion + 1

int main() {
  bool primes[100001];
  memset(primes,true,sizeof(primes));

  for(int i=2;i*i<=limit && primes[i]; ++i) {
    for(int j=i*i;j<=limit;j+=i) {
      primes[j]=false;
    }
  }

  vector<int> prim;
  prim.push_back(2);
  for(int i=3;i<=limit;i+=2) {
    if(primes[i])
      prim.push_back(i);
  }

  //  copy(prim.begin(),prim.end(),ostream_iterator<int>(cout,"\n"));

  int t;
  cin>>t;
  while(t--) {
    memset(primes,true,sizeof(primes));
    int m,n;
    cin>>m>>n;

    if(m<2)
      m=2;

    for(int i=0;i<prim.size();++i) {
      int t=prim[i];
      int start;

      if(t>=m)
	start=2*t;
      else
	start=m+((t-m%t)%t);
      for(int j=start;j<=n;j+=t) {
	primes[j-m]=false;
      }
    }

    int start=(m%2)?m:(m+1);

    if(m==2)
      cout<<"2\n";

    for(int i=start;i<=n;i+=2) {
      if(primes[i-m])
	cout<<i<<endl;
    }
    if(t)
      cout<<endl;
  }
  return 0;
}
