#include <iostream>
#include <cstdio>
#include <algorithm>
#include <stack>

using namespace std;

int main() {
  int t;
  cin>>t;
  string dummy;
  getline(cin,dummy);
  
  while(t--) {
    stack<char> s,q;
    string exp;
    getline(cin,exp);
    for(int i=0;i<exp.size();++i) {
      if(exp[i]>='a' && exp[i]<='z') {
	s.push(exp[i]);
      } else if(exp[i]=='(')
	continue;
      else if(exp[i]==')') {
	char t=q.top();
	q.pop();
	s.push(t);
      }
      else {
	q.push(exp[i]);
      }
    }
    string st="";
    while(!s.empty()) {
      st+=s.top();
      s.pop();
    }
    reverse(st.begin(),st.end());
    cout<<st<<endl;
  }
  return 0;
}
