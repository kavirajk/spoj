// Author: kaviraj
// Tag: math

#include <iostream>
#include <cstdio>
#include <cmath>

using namespace std;

int main() {
  int l;
  while(scanf("%d",&l)==1 && l) {
    double pi=acos(-1);
    double r=l/pi;
    double a=pi*r*r/2.0;
    printf("%.2f\n",a);
  }
  return 0;
}
