#include <iostream>
#include <cstdio>

typedef unsigned long long int64;

using namespace std;

int fastPow(int a,int64 b) {
  if(a==1)
    return 1;
  if(b==0)
    return 1;
  if(b==1)
    return a;
  int64 res=fastPow(a*a%10,b/2);
  if(b%2)
    return res*a%10;
  return res%10;
}

int main() {
  int t;
  cin>>t;
  while(t--) {
    string str;
    int a;
    int64 b;
    cin>>str;
    cin>>b;
    a=str[str.size()-1]-'0';
    cout<<fastPow(a,b)<<endl;
  }
  return 0;
}
