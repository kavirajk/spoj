#include <iostream>
#include <cstdio>
#include <algorithm>

using namespace std;

string multipleby2(string str) {
  reverse(str.begin(),str.end());
  int carry=0;
  string res="";
  for(int i=0;i<str.size();++i) {
    char c=str[i];
    int t=c-'0';
    t*=2;
    t+=carry;
    carry=t/10;
    t%=10;
    res+=('0'+t);
  }

  while(carry) {
    res+=('0'+carry%10);
    carry/=10;
  }

  reverse(res.begin(),res.end());
  return res;
}

string minusby2(string str) {
  reverse(str.begin(),str.end());
  int i;
  int t=str[0]-'0';
  if(t>=2)
    str[0]=t-2+'0';
  else {
    i=1;
    int n=str[i]-'0';
    while(n==0) {
      str[i]='9';
      i++;
      n=str[i]-'0';
    }
    str[i]=str[i]-1;
    str[0]=(str[0]-'0')+10-2+'0'; // little tricky, just think about how will you subtract last digit after borrowing
  }
  reverse(str.begin(),str.end());
  i=0;
  while(str[i]=='0' && str.size()!=1)
    ++i;
  return str.substr(i);
}

int main() {
  string n;
  while(cin>>n) {
    if(n=="0")
      cout<<0<<endl;
    else if(n=="1")
      cout<<1<<endl;
    else {
      string mul=multipleby2(n);
      string minus=minusby2(mul);
      cout<<minus<<endl;
    }
  }
  return 0;
}
