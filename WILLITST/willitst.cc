// Author: kaviraj
// Tag: bitwise

#include <iostream>
#include <cstdio>

using namespace std;

int main() {
  long long n;
  while(scanf("%lld",&n)==1) {
    if(!(n & (n-1))) // check n is power of 2. actuall checking should b "n && !(n & n-1)" but here this is sufficient
      printf("TAK\n");
    else
      printf("NIE\n");
  }
  return 0;
}
