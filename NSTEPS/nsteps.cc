#include <iostream>
#include <cstdio>

using namespace std;

int main() {
  int t;
  cin>>t;
  while(t--) {
    int x,y;
    cin>>x>>y;
    if(x==y || x-y==2) {

      if(x==y) {
	if(x%2==0)
	  cout<<2*x<<endl;
	else
	  cout<<x+(x-1)<<endl;
      }

      if(x-y==2) {
	if(y%2==0) {
	  cout<<x+y<<endl;
	} else
	  cout<<x+(y-1)<<endl;
      }

    } else {
      cout<<"No Number"<<endl;
    }
  }
  return 0;
}
