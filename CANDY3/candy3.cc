#include <cstdio>
#include <iostream>
#include <vector>
#include <numeric>

using namespace std;

int main() {
  int t;
  cin>>t;
  while(t--) {
    int n;
    cin>>n;
    int t,sum=0;
    for(int i=0;i<n;++i) {
      cin>>t;
      sum=(sum+t)%n;
    }
    if(sum==0)
      cout<<"YES\n";
    else
      cout<<"NO\n";
  }
  return 0;
}
