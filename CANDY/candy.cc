#include <iostream>
#include <cstdio>
#include <vector>
#include <numeric>

using namespace std;

int main() {
  int n;
  while(cin>>n && n!=-1) {
    vector<int> v(n);
    for(int i=0;i<n;++i) {
      cin>>v[i];
    }
    int tot=accumulate(v.begin(),v.end(),0);
    int each=tot/n;
    if(tot%n!=0)
      cout<<"-1\n";
    else {
      int ans=0;
      for(int i=0;i<n;++i) {
	if(v[i]>each)
	  ans+=(v[i]-each);
      }
      cout<<ans<<endl;
    }
  }
  return 0;
}
