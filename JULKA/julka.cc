#include <iostream>
#include <cstdio>
#include <algorithm>
#include <iterator>
#include <cstring>

using namespace std;

string sum(string s,string t) {
  int x[110],y[110],z[110];
  memset(x,0,sizeof(x));
  memset(y,0,sizeof(y));
  memset(z,0,sizeof(z));

  reverse(s.begin(),s.end());
  reverse(t.begin(),t.end());

  for(int i=0;i<s.size();++i) {
    x[i]=s[i]-'0';
  }
  for(int i=0;i<t.size();++i) {
    y[i]=t[i]-'0';
  }

  int lim=max(s.size(),t.size());
  int carry=0;
  int k;
  for(k=0;k<lim;++k) {
    z[k]=(x[k]+y[k]+carry);
    carry=z[k]/10;
    z[k]=z[k]%10;
  }
  while(carry) {
    z[k]=z[k]+carry;
    carry=z[k]/10;
    z[k]=z[k]%10;
    k++;
  }
  string ans="";
  for(int i=0;i<k;++i) {
    ans+='0'+z[i];
  }
  reverse(ans.begin(),ans.end());
  return ans;
}

string div2(string s) {
  //  reverse(s.begin(),s.end());
  int z[110],x[110];
  memset(z,0,sizeof(z));
  memset(x,0,sizeof(x));
  
  for(int i=0;i<s.size();++i) {
    z[i]=s[i]-'0';
  }

  int carry=0;

  for(int i=0;i<s.size();++i) {
    int t=carry*10+z[i];
    carry=(t)%2;
    x[i]=t/2;
  }

  string ans="";
  int k=0;
  while(x[k++]==0);
  for(int i=k-1;i<s.size();++i) {
    ans+='0'+x[i];
  }

  //  reverse(ans.begin(),ans.end());
  return ans;
}

string sub(string s,string t) {
  int x[110],y[110],z[110];
  memset(x,0,sizeof(x));
  memset(y,0,sizeof(y));
  memset(z,0,sizeof(z));

  reverse(s.begin(),s.end());
  reverse(t.begin(),t.end());

  for(int i=0;i<s.size();++i) {
    x[i]=s[i]-'0';
  }
  for(int i=0;i<t.size();++i) {
    y[i]=t[i]-'0';
  }

  for(int i=0;i<s.size();++i) {
    if(x[i]>=y[i])
      z[i]=x[i]-y[i];
    else {
      if(x[i+1]==0) {
	int k=i+1;
	while(x[k]==0){
	  x[k++]=9;
	}
	x[k]-=1;
      } else {
	x[i+1]-=1;
      }
      x[i]+=10;
      z[i]=x[i]-y[i];
    }
  }

  /*  cout<<"z: ";
  copy(z,z+s.size(),ostream_iterator<int>(cout,""));
  cout<<endl;
  */
  reverse(z,z+s.size());
  int k=0;
  while(z[k++]==0);
  string ans="";
  for(int i=k-1;i<s.size();++i) {
    ans+=z[i]+'0';
  }
  return ans;
}
int main() {
  int x[110],y[110],z[110],ans[110];
  string sx,sy;
  while(cin>>sx>>sy) {
    cout<<div2(sum(sx,sy))<<endl;
    cout<<div2(sub(sx,sy))<<endl;
  }
  return 0;
}
