#include <iostream>
#include <cstdio>
#include <vector>

using namespace std;

int main() {
  int n;
  while(cin>>n && n) {
    vector<int> s(n),t(n);
    for(int i=0;i<n;++i) {
      cin>>s[i];
      t[s[i]-1]=i+1;
    }
    if(s==t)
      cout<<"ambiguous\n";
    else
      cout<<"not ambiguous\n";
  }
  return 0;
}
