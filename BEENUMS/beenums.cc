#include <iostream>
#include <cstdio>
#include <algorithm>
#include <vector>

using namespace std;

vector<int> v;

int main() {
  v.push_back(1);
  v.push_back(7);
  int k=2;
  int i=6*k+v[v.size()-1];
  while(i<=1000000000) {
    v.push_back(i);
    k++;
    i=6*k+v[v.size()-1];
  }

  int n;
  while(cin>>n && n!=-1) {
    if(binary_search(v.begin(),v.end(),n)) {
      cout<<"Y\n";
    } else {
      cout<<"N\n";
    }
  }

  return 0;
}
