#include <iostream>
#include <cstdio>
#include <cmath>

using namespace std;

int main() {
  double n;
  while(cin>>n && n!=-1) {
    double k;
    k=(sqrt(1+4*((n-1)/3))-1)/2;
    if(k-int(k))
      cout<<"N\n";
    else
      cout<<"Y\n";
  }
  return 0;
}
