#include<iostream>
#include<cstdio>

using namespace std;

int main() {
  int T;
  cin>>T;
  while(T--) {
    int N;
    cin>>N;
    long n=N,ans=0;
    while((n/=5)) {
      ans+=n;
    }
    cout<<ans<<endl;
    
  }
  return 0;
}
