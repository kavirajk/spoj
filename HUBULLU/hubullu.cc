#include <iostream>
#include <cstdio>

using namespace std;

int main() {
  int t;
  cin>>t;
  while(t--) {
    int n,m;
    cin>>n>>m;
    if(m==0)
      cout<<"Airborne wins.\n";
    else
      cout<<"Pagfloyd wins.\n";
  }
  return 0;
}
