#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>

using namespace std;

int main() {
  int t;
  cin>>t;
  while(t--) {
    string s1,op1,s2,op2,s3;
    int a,b,c;
    a=b=c=0;
    cin>>s1>>op1>>s2>>op2>>s3;

    if(strchr(s1.c_str(),'m')==NULL)
      a=atoi(s1.c_str());

    if(strchr(s2.c_str(),'m')==NULL)
      b=atoi(s2.c_str());

    if(strchr(s3.c_str(),'m')==NULL)
      c=atoi(s3.c_str());
    
    if(a==0)
      a=c-b;
    else if(b==0)
      b=c-a;
    else
      c=a+b;

    cout<<a<<" + "<<b<<" = "<<c<<endl;

  }
  return 0;
}
