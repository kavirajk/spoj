#include <iostream>
#include <cstdio>
#include <cstdlib>

using namespace std;

int main() {
  int t;
  cin>>t;
  while(t--) {
    string s1,op1,s2,op2,s3;
    int a,b,c;
    a=b=c=0;
    cin>>s1>>op1>>s2>>op2>>s3;

    if(s1.find('m')==string::npos)
      a=atoi(s1.c_str());

    if(s2.find('m')==string::npos)
      b=atoi(s2.c_str());

    if(s3.find('m')==string::npos)
      c=atoi(s3.c_str());
    
    if(a==0)
      a=c-b;
    else if(b==0)
      b=c-a;
    else
      c=a+b;

    cout<<a<<" + "<<b<<" = "<<c<<endl;

  }
  return 0;
}
