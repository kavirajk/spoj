#include <iostream>
#include <cstdio>

using namespace std;

int main() {
  int t;
  cin>>t;

  while(t--) {
    int n;
    cin>>n;

    int i=1;
    int sum=0;
    while(true) {
      sum+=i;
      if(sum>=n)
	break;
      i++;
    }

    int tmp=n-(sum-i);
    int total=i+1;

    if(total%2)
      cout<<"TERM "<<n<<" IS "<<tmp<<"/"<<total-tmp<<endl;
    else
      cout<<"TERM "<<n<<" IS "<<total-tmp<<"/"<<tmp<<endl;
  }
  return 0;
}
