#include <iostream>
#include <cstdio>
#include <cstdlib>

using namespace std;

int main() {
  int T;
  cin>>T;

  while(T--) {
    int n,carry=0,k=0;
    int arr[1000]={1};
    cin>>n;
    for(int i=1;i<=n;++i) {
      for(int j=0;j<=k;++j) {
	arr[j]=arr[j]*i+carry;
	carry=arr[j]/10;
	arr[j]=arr[j]%10;
      }
      while(carry) {
	k++;
	arr[k]=carry%10;
	carry/=10;
      }
    }
    for(int i=k;i>=0;--i) {
      cout<<arr[i];
    }
    cout<<endl;
  }
  return 0;
}
