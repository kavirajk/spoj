#include <cstdio>
#include <iostream>

using namespace std;

int main() {
  int n;
  cin>>n;

  int ans=0;

  for(int i=1;i<=10000;++i) {
    for(int j=i;j<=10000;++j) {
      if(i*j<=n)
	ans++;
    }
  }
  cout<<ans<<endl;
  return 0;
}
